$(document).ready(function(){
    $('.container_slider').slick({
        autoplay:false,
        autoplaySpeed:3000,
        arrows: false
    });

    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
});



let burger = document.querySelector(".burger");
let headernav = document.querySelector(".header-nav");

burger.onclick = function (){
    headernav.classList.toggle('vis');
}

const smoothLinks = document.querySelectorAll('a[href^="#"]');
for (let smoothLink of smoothLinks) {
    smoothLink.addEventListener('click', function (e) {
        e.preventDefault();
        const id = smoothLink.getAttribute('href');

        document.querySelector(id).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
};
